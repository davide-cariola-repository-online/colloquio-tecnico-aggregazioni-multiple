<?php

namespace Database\Seeders;

use Carbon\Carbon;
use App\Models\User;
use App\Models\Project;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class TaskSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = ['Mario', 'Giovanni', 'Lucia'];

        foreach ($users as $user) {
            \App\Models\User::factory()->create([
                'name' => $user
            ]);
        }

        $projects = ['Mars Rover', 'Manhattan'];

        foreach ($projects as $project) {
            \App\Models\Project::factory()
            ->create([
                'name' => $project
            ]);
        }

        $dates = [
            Carbon::now(),
            Carbon::now()->subWeek(),
            Carbon::now()->subMonth(),
        ];

        \App\Models\Task::factory(6)
        ->state(new Sequence(
            fn(Sequence $sequence) => [
                'employee_id' => User::all()->random(),
                'project_id' => Project::all()->random(),
                'date' => $dates[$sequence->index % 3],
            ],
        ))
        ->create();
    }
}
