<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Task>
 */
class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'project_id' => fake()->randomNumber(1) + 1,
            'employee_id' => fake()->randomNumber(1) + 1,
            'date' => fake()->dateTime(),
            'hours' => fake()->randomNumber(1) + 1,
        ];
    }
}
