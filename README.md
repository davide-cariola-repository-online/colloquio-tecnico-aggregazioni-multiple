<p align="center">
  <a href="https://davidecariola.it" rel="noopener">
 <img width=50px src="https://davidecariola.it/media/logos/new-logo-resize.webp" alt="Davide Cariola"></a>
</p>

<h2 align="center">Aggregazioni Multiple</h3>

<p align="center"> Traccia: in azienda esiste un sistema di raccolta dei dati relativi alle ore lavorative spese da ogni impiegato su diversi progetti. Si necessita la creazione di una pagina web che recuperi questi dati e ne permetta l'aggregazione su uno piu' campi contemporaneamente.

La pagina web consente di aggregare per uno o piu' campi tra:
* progetto
* impiegato
* data

Quando l'utente sceglie un'aggregazione, il numero delle ore delle attivita' aggregate viene sommato.

E' importante considerare che l'ordine con cui vengono scelti i campi per l'aggregazione produce risultati diversi nell'ordinamento di colonne e righe.

Il progetto e' stack-agnostic. Il database puo' essere simulato con una variabile in memoria.
    <br> 
</p>

## 📝 Table of Contents

- [About](#about)
- [Usage](#usage)
- [Built Using](#built_using)
- [Author](#author)

## 🧐 About <a name = "about"></a>

Questo progetto nasce come risoluzione di una traccia di colloquio tecnico per una software house.

### Prerequisites

1. php: ^8.1
2. laravel/framework: ^10
3. laravel/livewire: ^3.4

### Installing

Clona il progetto. Poi, nel terminale, assicurati di essere nella root del progetto e copia il file `.env.example` in un nuovo file `.env.`

```
cp .env.example .env
```

Nel file `.env` puoi impostare il tuo database preferito o anche usare SQLite.

Installa poi le dipendenze di composer e npm

```
composer i && npm i
```

Fatto questo, lancia le migrazioni.

```
php artisan migrate
```

e lancia anche il seeding del database. Hai due scelte.

Per creare dati casuali

```
php artisan db:seed
```

Per creare dati sempre leggibili

```
php artisan db:seed --class=TaskSeeder
```

Adesso puoi esplorare il progetto.

## 🎈 Usage <a name="usage"></a>

Sulla homepage vedrai una serie di opzioni e una tabella. Al cambiare delle opzioni, i dati nella tabella cambiano e vengono aggregati in maniera diversa.


## ⛏️ Built Using <a name = "built_using"></a>

- [SQLite](https://www.sqlite.org/index.html)
- [Laravel](https://laravel.com/)
- [Laravel Livewire](https://livewire.laravel.com)

## ✍️ Author <a name = "authors"></a>

- [Davide Cariola](https://davidecariola.it)

<br>