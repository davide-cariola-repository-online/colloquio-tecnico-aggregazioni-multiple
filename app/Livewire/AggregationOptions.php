<?php

namespace App\Livewire;

use Livewire\Component;

class AggregationOptions extends Component
{
    public function aggregate($field)
    {
        $this->dispatch('toggle-aggregation', $field);
    }

    public function render()
    {
        return view('livewire.aggregation-options');
    }
}
