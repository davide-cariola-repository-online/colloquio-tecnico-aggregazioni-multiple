<?php

namespace App\Livewire;

use App\Models\Task;
use App\Services\TaskQueryService;
use Livewire\Component;
use Livewire\Attributes\On;

class TasksTable extends Component
{
    public $aggregatedFields = [];
    protected TaskQueryService $queryService;

    #[On('toggle-aggregation')]
    public function onToggleAggregation(string $field): void
    {
        if(in_array($field, $this->aggregatedFields)){
            $this->aggregatedFields = array_diff($this->aggregatedFields, [$field]);
        } else {
            $this->aggregatedFields[] = $field;
        }
    }

    public function boot(TaskQueryService $queryService): void
    {
        $this->queryService = $queryService;
    }

    public function render()
    {
        $tasks = $this->queryService->getTasks($this->aggregatedFields);

        return view('livewire.tasks-table', [
            'tasks' => $tasks,
        ]);
    }
}
