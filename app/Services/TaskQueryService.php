<?php

namespace App\Services;

use App\Models\Task;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class TaskQueryService
{
    /**
     * Costruisce la query a seconda dell'aggregatore scelto
     * @param array $aggregatedFields
     * @return Builder
     */
    private function buildTasksQuery(array $aggregatedFields): Builder
    {
        $tasksQuery = Task::query();

        if (!empty($aggregatedFields)) {
            $tasksQuery->selectRaw('*, SUM(hours) as total_hours');

            foreach ($aggregatedFields as $field) {
                $groupByField = $field === 'date' ? $field : $field . '_id';
                $tasksQuery->groupBy($groupByField);
            }
        }

        return $tasksQuery;
    }

    /**
     * Esegue la query creata
     * @param array $aggregatedFields
     * @return Collection
     */
    public function getTasks(array $aggregatedFields): Collection
    {
        $tasksQuery = $this->buildTasksQuery($aggregatedFields);

        return $tasksQuery->get();
    }
}
