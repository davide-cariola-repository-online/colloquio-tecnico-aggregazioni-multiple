<x-layouts.app>
    <div class="container-fluid p-5 bg-light text-center">
        <div class="row">
            <div class="col-12">
                <h1 class="display-1">
                    Aggregazioni multiple
                </h1>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-8">
                @livewire('aggregation-options')
                @livewire('tasks-table')
            </div>
        </div>
    </div>
</x-layouts.app>