<table class="table table-striped border">
    <thead class="border border-2">
        <tr>
            @forelse($aggregatedFields as $field)
                <th scope="col">{{ ucfirst($field) }}</th>
            @empty
                <th scope="col">Project</th>
                <th scope="col">Employee</th>
                <th scope="col">Date</th>
            @endforelse
            <th scope="col">Hours</th>
        </tr>
    </thead>
    <tbody>
        @foreach($tasks as $task)
            <tr>
                @forelse($aggregatedFields as $field)
                    <td>
                        {{ $task->$field->name ?? $task->date}}
                    </td>
                @empty
                    <td>{{ $task->project->name }}</td>
                    <td>{{ $task->employee->name }}</td>
                    <td>{{ $task->date }}</td>
                    <td>{{ $task->hours }}</td>
                @endforelse

                @isset($task->total_hours)
                    <td>{{ $task->total_hours }}</td>
                @endisset
            </tr>
        @endforeach
    </tbody>
</table>
