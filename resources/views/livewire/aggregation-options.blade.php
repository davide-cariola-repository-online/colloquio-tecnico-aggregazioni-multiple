<div class="mb-4">
    <h3 class="mb-3">
        Opzioni di aggregazione
    </h3>
    
    <x-button functionName="aggregate('project')" text="Project"/>
    <x-button functionName="aggregate('employee')" text="Employee"/>
    <x-button functionName="aggregate('date')" text="Date"/>
</div>
