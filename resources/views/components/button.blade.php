<div class="d-inline" x-data="{ active: false }">
    <button wire:click="{{ $functionName }}" class="btn me-2" :class="active ? 'btn-secondary' : 'btn-outline-secondary'" x-on:click="active = !active">
        {{ $text }}
    </button>
</div>